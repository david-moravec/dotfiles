{
  "workbench.colorTheme": "Dracula",
  "editor.fontFamily": "Roboto Mono",
  "editor.fontSize": 13,

  "editor.formatOnSave": true,
  "editor.rulers": [79],
  "editor.minimap.enabled": false,
  "prettier.singleQuote": true,
  "prettier.trailingComma": "all",
  "prettier.semi": false,
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.lineNumbers": "relative",
  "gitlens.codeLens.authors.command": false,
  "gitlens.codeLens.authors.enabled": false,
  "gitlens.showWelcomeOnInstall": false,
  "gitlens.hovers.currentLine.over": "line",
  "gitlens.currentLine.enabled": false,
  "cmake.configureOnOpen": true,
  "gitlens.views.remotes.branches.layout": "list",
  "window.zoomLevel": -1
}
